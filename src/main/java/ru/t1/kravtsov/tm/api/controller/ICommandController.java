package ru.t1.kravtsov.tm.api.controller;

public interface ICommandController {

    void displayWelcome();

    void displayHelp();

    void displayArguments();

    void displayCommands();

    void displayVersion();

    void displayAbout();

    void displayArgumentError();

    void displayCommandError();

}
