package ru.t1.kravtsov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display developer info.";

    public static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Alexander Kravtsov");
        System.out.println("EMAIL: aekravtsov@nota.tech");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
