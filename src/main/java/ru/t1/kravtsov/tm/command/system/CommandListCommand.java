package ru.t1.kravtsov.tm.command.system;

import ru.t1.kravtsov.tm.command.AbstractCommand;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-c";

    public static final String DESCRIPTION = "Display application commands.";

    public static final String NAME = "commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (AbstractCommand command : getCommandService().getTerminalCommands()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
